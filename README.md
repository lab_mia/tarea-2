# Tarea No. 2
Esta tarea tiene como objetivo familiarizar al estudiante con el uso de las estructuras internas de un sistema de 
archivos ext2. Entiendase, los bitmaps de inodos, los bitmaps de bloques, los inodos, bloques de datos,
bloques de carpeta y bloques de indirección. 

## Contenido del repositorio.
En el repositorio encontraran un archivo binario nombrado ```disk```. Dentro encontraran  una implementacion trivial basada en el sistema de archivos ext2. 
Las diferencias entre la implementación real y la implementacion a modo de ejemplo que ustedes utilizarán
para la tarea son las siguientes.

1. El no. de bloques y el no. de inodos fue definido de forma arbitraria y no en base al tamaño de la partición.
2. No hay superbloque. Por simpleza y que ustedes no pierdan mucho tiempo en la tarea, el superbloque se omitió
del archivo. El header denominado ```handicap.h``` Es un
header que define varias constantes que los ayudan a obtener la información respecto a la partición que van 
a necesitar para desarrollar la tarea y hacer uso del sistema. 
En una implementacion real, las constantes definidas en handicap.h deberían ser obtenidas del super bloque y no son
constantes, sino que se definen al momento de formatear el disco en base a la información de este.
3. El archivo se llama ```disk```. Sin embargo, realmente hace referencia a una partición, no a un disco. 
Pues un disco u otro dispositivo de almacenamiento secundario, si fuera
en la vida real, primero hay que particionarlo, antes de formatear un sistema de archivos. Aún si el superbloque
estuviera presente, en la vida
real un sistema de archivos nunca se escribe directamente a un dispositivo de almacenamiento secundario como lo es un disco. Siempre debe particionarse primero. 

### Esquema
El esquema que sigue la implementacion de ejemplo que ustedes van a manejar es una versión simplificada del sistema ext2 y tiene la siguiente forma:

#### Offset: 0
En el offset 0, lo primero que van a encontrar es el bitmap de inodos, definido como un arreglo de char con longitud inode_count;

#### Offset: SEEK_SET + inode_count
Lo siguiente que van a encontrar y que se encuentra en este offset es el bitmap de bloques. Definido como un arreglo de char de longitud block_count;

#### Offset: SEEK_SET + inode_count + block_count
En este offset, lo siguiente que econtraran son los N inodos del disco. Uno tras otro. Todos inicializados previamente. Donde N esta definido por inode_count;
#### Offset: SEEK_SET + inode_count + block_count + inode_count*sizeof(inode)
En este offset comienza la última parte del archivo, en esta parte se encuentran los M bloques. Todos inicializados con valor nulo; M está definido por block_count;
<br>
<br>
Luego de este offset se termina el archivo. SEEK_SET equivale a 0 y se refiere a que los offsets indicados comienzan desde el principio del archivo.

### estructuras.h
En este archivo encuentran un header que define cada una de las estructuras utilizadas en la tarea. Deben utilizarlo para poder leer las estructuras del archivo y que
no tengan problemas de offset o compatibilidad.

### read
Este es un ejecutable, pueden ejecutarlo con el comando ```./read disk ``` donde disk es el primer parametro e indica el path hacia el archivo que va a leer. Por default
buscara en el directorio actual. Si el sistema operativo les muestra un error de permisos de ejecucion, pueden solucionarlo con el comando ```chmod +x ./read``` <br>
Este ejecutable lee el archivo disk, abre la carpeta raiz dentro del disco 
y busca todos los archivos que existan. Por cada archivo, junta los bloques de datos e imprime un nuevo
archivo en el sistema operativo real con el nombre que tiene su version virtual y por contenido el contenido que encontro dentro del disco. <br>
El disco que ustedes encontraran en este repositorio tiene 16 archivos, pertenecientes a 16 alumnos voluntarios durante el laboratorio. La tarea de ustedes es 
simplemente modificar el disco, de modo que además de los 16 archivos que ya tiene, agreguen uno nuevo con path /CARNET. Donde el CARNET es su número de carnet y
el contenido  del archivo  que van a escribir es  el contenido del archivo contenido.tex que también
se encuentra en este repositorio; <br>

Para verificar que hayan hecho bien su tarea, simplemente ejecuten el ejecutable read. En vez de generar 16 archivos, debería generar 17 que pertenecen a los 16 
originales + el archivo que acaban de agregar.

## Entregables
El entregable que deben subir a Uedi no es nada más que el archivo binario disk. Yo me encargaré de leer el archivo y recuperar todos los archivos que existan dentro
del disco, para obtener los 16 originales y el nuevo que agregaron como parte de su tarea. NO deben eliminar los 16 originales y dejar solo el suyo o sobreescribir
alguno. El contenido del archivo es importante! No vayan a crear un archivo vacío en root. Me asegurare que el contenido del archivo que estedes hayan creado
sea el contenido del archivo contenido.tex que está en este repositorio.

## Fecha de entrega 
La fecha de entrega es el Sábado 04 de Septiembre a media noche.

## Notas
- Pueden utilizar C o C++. El ejecutable que ustedes encuentran en este repositorio fué desarrollado con C. 
- El archivo de entrada ocupa 2 niveles de indirección. Por lo que no es necesario que implementen los 3 niveles para la tarea.
- Es importante que si utilicen los headers de estructuras y constantes para que puedan navegar sobre el disco.
- Si Uedi no les deja subir archivos binarios, pueden envolver el archivo disk en un archivo comprimido .zip

## Voluntarios
Los 16 alumnos voluntarios que no necesitarán hacer la tarea son los siguientes:
- 201612132
- 201901604
- 201115018   
- 201901016
- 201902714   
- 201801528
- 201903850
- 201531166
- 201602938
- 201602894
- 201906180
- 201709450
- 201800712
- 199213640
- 199817948
- 201903004
