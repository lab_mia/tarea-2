typedef struct {
  /* This is a homework. It is a trivial implementation compared to the project.
   * The fields:
   * uid
   * gid
   * atime
   * ctime
   * mtime
   * perm
   * Were omitted for simplicity. You must implement them all in the project though. */
    int size;
    int blocks[16];
    char type;
} inode;

typedef struct {
 char name[12];
 int inodo;
} content;

 typedef struct {
   content contenido[4];
 } carpeta;

typedef struct {
  char contenido[64];
} block;

typedef struct {
  int pointers[16];
} indirecto;
